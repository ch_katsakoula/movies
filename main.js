// TODO on click: back
$(document).ready(function () {

    //infinite scroll
    let pageCounter = 1;
    $(window).scroll(function () {
        console.log('scroll');

        if ($(window).scrollTop() == 0)
            return;
        else if ($(window).scrollTop() > $(document).height() - $(window).height() - 10) {
            pageCounter++;
            console.log('scroll trigger');

            Movies.Ui.displayTemplate({ templateID: 'm-card-template', container: 'cardsDiv', isScrolling: false }, { search: $('#searchInput').val(), pages: pageCounter })
        }
        console.log('scroll true');
    });

    //searchInput
    let timerId;
    $('#searchInput').on('input', function () {
        clearTimeout(timerId);
        timerId = setTimeout(() => {
            Movies.Ui.displayTemplate({ templateID: 'm-card-template', container: 'cardsDiv', isScrolling: false }, { search: this.value });
        }, 500)
    });

    //on click: details
    $('#cardsDiv').on('click', '.item-card', function (event) {

        let itemCardDivID = event.target.parentElement.parentElement.id;
        Movies.Ui.displayTemplate(
            { templateID: 'm-details-template', container: 'detailsDiv', isScrolling: false },
            { id: itemCardDivID });
    });

    //on click: back
    $('#detailsDiv').on('click', '.item-details', function (event) {  
        console.log('working');
    })
    $('#detailsDiv').on('click', '.back-btn', function (event) {  
        console.log('working');
    })

    //on click: home
    $('#homepage').on('click', function () {
        Movies.Ui.displayTemplate({ templateID: 'm-card-template', container: 'cardsDiv', isScrolling: false }, {});
        $('#searchInput').val('');
    });

    // initial search
    window.onload = Movies.Ui.displayTemplate(
        { templateID: 'm-card-template', container: 'cardsDiv', isScrolling: false }, {});

    // Movies.Ui.displayTemplate('m-details-template', { id: 299536 }, false);

});