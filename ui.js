// TODO change names everywhere

(function (Movies) {
    Movies.Ui = Movies.Ui || {};

    let replaceNulls = function (prop) {
        prop = prop ? prop : "https://cdn.browshot.com/static/images/not-found.png";
        prop = prop == "https://cdn.browshot.com/static/images/not-found.png" ? prop : 'https://image.tmdb.org/t/p/original' + prop;
        return prop;
    }


    let uiFormats = function (array) {
        array.genres = [{ "id": 28, "name": "Action" }, { "id": 12, "name": "Adventure" }, { "id": 16, "name": "Animation" }, { "id": 35, "name": "Comedy" }, { "id": 80, "name": "Crime" }, { "id": 99, "name": "Documentary" }, { "id": 18, "name": "Drama" }, { "id": 10751, "name": "Family" }, { "id": 14, "name": "Fantasy" }, { "id": 36, "name": "History" }, { "id": 27, "name": "Horror" }, { "id": 10402, "name": "Music" }, { "id": 9648, "name": "Mystery" }, { "id": 10749, "name": "Romance" }, { "id": 878, "name": "Science Fiction" }, { "id": 10770, "name": "TV Movie" }, { "id": 53, "name": "Thriller" }, { "id": 10752, "name": "War" }, { "id": 37, "name": "Western" }];


        if (array.results) {
            for (let i = 0; i < array.results.length; i++) {
                array.results[i].backdropUrl = replaceNulls(array.results[i].backdrop_path);
                array.results[i].posterUrl = replaceNulls(array.results[i].poster_path);
                array.results[i].movieGenres = [];
                array.results[i].genre_ids.map(obj =>{ 
                    if (obj.id == array.genres['id']) {
                        // let ind = array.genres.indexOf(id);

                        array.results[i].movieGenres.push({category: array.genres['id']}) 
                    }
                 });
            }
        }
        else {
            array.backdropUrl = replaceNulls(array.backdrop_path);
            array.posterUrl = replaceNulls(array.poster_path);
            array.imdbUrl = 'https://www.imdb.com/title/' + array.imdb_id;

            array.release_date = new Date(array.release_date);
            array.formatedDate = dateFormat(array.release_date, 'mmmm d, yyyy')
        }
        return array;
    }

    let _displayTemplate = function ({ templateID, container, isScrolling }, data) {

        data = uiFormats(data);
        // handlebars template compile
        let template = document.getElementById(templateID).innerHTML;
        let info = Handlebars.compile(template);
        let dataArray = info({
            res: data,
        });
        console.log('formated data', data);

        // deactivate the previous template
        let deactivation = container == 'cardsDiv' ? '#detailsDiv' : '#cardsDiv';
        $(deactivation).hide();
        $(`#${container}`).show();

        // check if scrolling
        if (templateID == 'm-details-template' || !isScrolling) {
            document.getElementById(container).innerHTML = '';
        }

        document.getElementById(container).innerHTML += dataArray;
    };

    Movies.Ui.displayTemplate = function (displayParams, urlParams) {
        Movies.Api.getData(urlParams).done(function (data) {
            _displayTemplate(displayParams, data);
        });
    };

    return Movies;
})(window.Movies || {});