// TODO: errorHandler

(function (Movies) {
    Movies.Api = Movies.Api || {};

    function createUrl({ id = 'popular', lang = 'en-US', search = '', pages = 1, isAdult = false }) {

        let actSearch = search ? 'search/movie' : 'movie/' + id;
        // let apiKey = '93902e241dca1f4b1573663a83f3f496';

        return `https://api.themoviedb.org/3/${actSearch}?api_key=93902e241dca1f4b1573663a83f3f496&&language=${lang}&query=${search}&page=${pages}&include_isAdult=${isAdult}`;
    };

    Movies.Api.getData = function ({ id, lang, search, pages, isAdult }) {
        console.log('new req' ,id, lang, search, pages, isAdult);
        
        return $.get(createUrl({ id, lang, search, pages, isAdult }));
    };
    return Movies;

})(window.Movies || {});
